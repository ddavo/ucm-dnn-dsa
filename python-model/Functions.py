from math import e

def relu(x, t=0, c = 0, m = 1):
    return m*x if x > t else c

def sigmoid(x):
    return 1 / (1 + pow(e, x))

def step(x, t=0):
    return 1 if x > t else 0
