from Functions import *

class Node:
    def getOutput():
        pass

class SimpleInput(Node):
    def __init__(value):
        self.output = value

    def getOutput():
        return self.value

class SigmoidPerceptroin(Node):
    def __init__(bias = 0):
        self.inputs = {}
        # We use RELU by default
        self.sigmoidFunction = relu
        self.bias = bias

    def addInput(node, weight):
        if not node in self.inputs:
            self.inputs[node] = weight

    def setBias(bias):
        self.bias = bias

    def removeInput(node):
        del self.inputs[node]

    def setSigmoidFunction(lfunction):
        self.sigmoidFunction = lfunction

    def getOutput():
        total = self.bias
        for node,weight in self.inputs.items():
            total += node.getOutput() * weight;

        return self.sigmoidFunction(total)

